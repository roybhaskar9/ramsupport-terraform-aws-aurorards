resource "aws_rds_cluster_instance" "cluster_instances" {
  identifier         = "${var.cluster_name}-instance"
  cluster_identifier = "${aws_rds_cluster.cluster.id}"
  instance_class     = "${var.instance_class}"
}

resource "aws_rds_cluster" "cluster" {
  cluster_identifier     = "${var.cluster_name}"
  database_name          = "sample_rds"
  master_username        = "${var.username}"
  master_password        = "${var.password}"
  vpc_security_group_ids = ["${aws_security_group.aurora-sg.id}"]
  skip_final_snapshot    = true
}

resource "aws_security_group" "aurora-sg" {
  name   = "aurora-security-group"
  vpc_id = "${aws_default_vpc.default.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0 
    to_port     = 0 
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "null_resource" "setup_ssh_tunnel" {
  depends_on = ["aws_rds_cluster_instance.cluster_instances"] #wait for the db to be ready
  provisioner "local-exec" {
    command = "ssh -L 3307:${aws_rds_cluster_instance.cluster_instances.endpoint}:3306 ubuntu@replace_this_text_with_bastion_host_dns -i replace_this_text_with_path_to_sshkey_for_bastion_host -N"
    #example: ssh -L 3307:rds-sample-cluster-instance.cn03ausgmvh3.us-east-1.rds.amazonaws.com:3306 ubuntu@ec2-52-91-54-64.compute-1.amazonaws.com -i ./example.pem -N
  }
}

resource "null_resource" "setup_db" {
  depends_on = ["null_resource.setuo_ssh_tunnel"] #wait for the ssh tunnel to be ready
  provisioner "local-exec" {
    command = "mysql -u ${var.username} -p${var.password} -P 3307 -h 127.0.0.1 < file.sql"
  }
}